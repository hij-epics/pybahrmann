/*
SMS-Library for using SMS-CONTROLLER(UNI-JENA) over Network

created at 16.05.2023 by A. Kessler a.kessler@hi-jena.gsi.de


based on SMSLib.py Created on Wed Jul 14 11:34:19 2021 by schulze & bahrmann
Included only VC-1 Commands and Parameters !!!!
*/


#define _WINSOCK_DEPRECATED_NO_WARNINGS
#pragma comment(lib,"ws2_32.lib")
#include <stdio.h>
#include <WinSock2.h>
#include <windows.h>
#include "SMSLib.h"
#include <cmath>
//#include <string>

#define BUFSIZE 1024

const char smsIP[] = "192.168.55.120";
int	error;
int const port = 5000;
Motorcodes motorID = MOTOR5;
char cmd[2];
char in[10];
const char handshake_list[] = { 0xFF, 0xE1, 0xE2, 0xE3, 0xE4, 0xE5, 0xE6, 0xE7, 0xE8, 0xE9, 0xEA, 0xEB, 0xEC, 0xED, 0xEE, 0xEF };
const char *errors[][2] = {
		{"0xE1", "Steuerung nicht im PC-Modus"},
		{"0xE2", "Befehlsspeicher voll"},
		{"0xE3", "keine Daten vorhanden"},
		{"0xE4", "Motor noch aktiv"},
		{"0xE5", "falsches Datenformat"},
		{"0xE6", "Enkontakte ausgeloest"},
		{"0xE7", "falscher Befehl"},
		{"0xE8", "Modul/Motor nicht vorhanden"},
		{"0xE9", "Fernbedienung defekt"},
		{"0xEA", "Fehler bei der Datenuebertragung"},
		{"0xEF", "Kurzschluss Motor"},
		{"0xFF", "kein Fehler"},
		{"0xF1", "Verbindung zur Steuerung unterbrochen"},
		{"0xF2", "Client konnte nicht gefunden werden"},
		{"0xF3", "keine Steuerung gefunden"},
		{"0xF4", "Anzahl Schritte falsch"}
};

char* errorCode2Msg(int errorCode)
{
	char hexStr[] = "0x00";
	snprintf(hexStr, sizeof(hexStr), "0x%X", errorCode);
	return hexStr;
}

int send_data2SMS(const char* bytes2send, int nrOfBytes,  char* response, int sizeofResponse, SOCKET s)
{
	int error = 0;
	for (int i = 0; i < nrOfBytes; i++) printf("Bytes to SMS [%d] = 0x%X \n", i, bytes2send[i]);
	int sent = send(s, bytes2send, nrOfBytes, FLAGS);
	// Nachricht senden
	if (sent == SOCKET_ERROR) {
		printf("Send failed. Error Code: %d\n", WSAGetLastError());
		return SOCKET_ERROR;
	}

	// Daten empfangen
	int data_len = 0;
	int recv_len = 0;  
	bool found = false;
	while (!found)
	{
		recv_len = recv(s, response + data_len, sizeofResponse - data_len, FLAGS);
		if (recv_len == SOCKET_ERROR) {
			printf("Receive failed. Error Code: %d\n", WSAGetLastError());
			error = 1;
			break;
		}
		else if (recv_len == 0) {
			printf("Connection closed by remote host.\n");
			error = 1;
			break;
		}
		else {
			data_len += recv_len;
		}

		// Softhandshake auswerten: Enweder OK = 0xFF oder Fehlermeldung.
		for (int i = 0; i < sizeof(handshake_list) / sizeof(handshake_list[0]); i++)
		{
			for (int k = 0; k < data_len / sizeof(response[0]); k++)
			{
				found = (response[k] == handshake_list[i]);
				if (found)
				{
					if (response[k] == (char)0xFF) //Es wichtig, dass man gleiche Datentypen vergeicht. ubyte!= byte !!!
						error = 0;					// Anm.: Das hängt wohl vom Compiler ab...
					else
					{
						error = response[k];
						printf("Error beim Datensenden: %x", error);
					}
					goto endsearch; // aus den verschachtelten Schleifen ausbrechen.
				}
			}
		}
	}
	endsearch:
	return error;
}


int writeParams2SMS(Motorcodes motorID, int steps, Stepcodes stepwidth, Rampencodes rampe, Frequenzcodes velo, SOCKET s)
{
	char response[BUFSIZE];
	char params[9];
	params[0] = motorID + 0x10;
	if (steps < 0)
	{
		steps = steps * (-1);
		params[6] = stepwidth;
	}
	else
	{
		params[6] = stepwidth + 0x01;
	}
		
	// Die Schritziffern werden hexadezimal in umgekehrter Reihenfolge in Bytes 1..5 übertragen
	for (int i = 0; i < 5; i++) params[i+1] = (steps >> (i*4)) & 0xF ; 
	
	params[7] = rampe;
	params[8] = velo;

	//send parameters to Motor
	return send_data2SMS(params, sizeof(params), response, sizeof(response), s);
}

int sendCmd2SMS(Motorcodes motorID, Commandcodes cmd, char* response, int sizeofResponse, SOCKET s)
{
	char params[] = { (char)motorID, (char)cmd };
	int error = send_data2SMS(params, sizeof(params), response, sizeofResponse, s);
	for (int i=0; i < 8; i++) printf("response[%d] = 0x%X \n", i, response[i]);
	return error;
}

int runMotor(Motorcodes motor, char* response, int sizeofResponse, int steps, SOCKET s)
{
	int error = 0;
	if (steps > 0xFFFFF || steps < -1048575 || steps == 0) // FFFFF = 1048575
	{
		printf("Motor error: %s\n", "Ungueltige Schrittzahl");
		error = 1;
		return error;
	}

	error = writeParams2SMS(motorID, steps, STEPTYPE_1_1, RAMPE1, F1000Hz, s);
	if(!error) error = sendCmd2SMS(motor, RUN_STEPS, response, sizeofResponse, s);
	if (error) printf("Motor error: %s\n", "fehler beim Fahren des Motors");

	return error;
}

int getState(Motorcodes motorID, Status* status, SOCKET s) 
{
	int error = 0;  // Standardwerte einstellen
	int pos = 0;
	char response[BUFSIZE];
	sendCmd2SMS(motorID, GET_STATUS, response, BUFSIZE,  s);  // Motorkommandosteuerwort + Kommando
	error = (response[9] != (char) 0xFF) ? 1 : 0;

	if (!error) 
	{
		status->position = 0;
		// Wenn z.B. 0x37 von SMS empfangen wird, dann ist das als dezimal 7 zu verstehen  
		// wenn aber gesendet wird, dann sehen die Bytes so aus 0x00...0x0F   ¯\_(ツ)_/¯
		for (int i = 0; i < 7; i++) status->position += ((int)response[i]-0x30)*std::pow(10,i);

		char statusbyte = response[7];
		status->endright = (statusbyte && 0x01);  // Endkontakt rechts
		status->endleft = (statusbyte && 0x02);   // Endkontakt links
		if(statusbyte && 0x04) status->position *= (-1);  // Vorzeichen
		status->motor_on = (statusbyte && 0x08) ? 1 : 0;   // Motorstatus -> 1= nicht bereit
		status->microstep_on = (statusbyte && 0x10) ? 1 : 0;  // Microschritte aktiviert
		status->softend_on = (statusbyte && 0x20) ? 1 : 0;   // Softendkontakte aktiviert
		printf("Motor %d: Position = %d \n", motorID-0x10, status->position );
	}	
	return error;
}

int connect2SMS(const char* IP, const int port, SOCKET* s)
{
	// initialise WinSocket
	WORD wVersionRequested = MAKEWORD(2, 2); // determine the version windows socket ===> 2
	WSAData lpwsdata; // pointer to stock all informations socket
	error = WSAStartup(wVersionRequested, &lpwsdata);
	if (error != false) {
		printf("WSAStartup failed with error : %d\n", error);
		return end;
	}


	// create socket
	*s = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);
	if (*s == INVALID_SOCKET) {
		printf("Socket failed with error : %d\n", WSAGetLastError());
		return end;
	}
	struct hostent* host_entry = gethostbyname(smsIP);
	// struct to stock IP + PORT + Version Protocol(IPv4,IPv6,...)
	struct sockaddr_in serverInformation;
	serverInformation.sin_family = AF_INET; // AF_INET = IPv4
	serverInformation.sin_addr.s_addr = *(unsigned long*)host_entry->h_addr;
	serverInformation.sin_port = htons(port); // port

	int timeout = 1000; // Timeout-Wert in Millisekunden (hier: 1 Sekunde)
	setsockopt(*s, SOL_SOCKET, SO_RCVTIMEO, (char*)&timeout, sizeof(timeout));
	setsockopt(*s, SOL_SOCKET, SO_SNDTIMEO, (char*)&timeout, sizeof(timeout));


	// connect &s with server
	return connect(*s, (sockaddr*)&serverInformation, sizeof(serverInformation));
}

int getCounter(Motorcodes motor, int* pCounter, SOCKET s)
{
	Status st;
	int error = getState(motorID, &st, s);
	if (!error)	*pCounter = st.position;
	return error;
}

int zeroCounter(Motorcodes motorID, SOCKET s)
{
	char response[BUFSIZE];
	return sendCmd2SMS(motorID, RESET, response, sizeof(response), s);
}

int go2zero(Motorcodes motorID, SOCKET s)
{
	char response[BUFSIZE];
	return sendCmd2SMS(motorID, GO_ZERO, response, sizeof(response), s);
}

int go2posEC(Motorcodes motorID, SOCKET s)
{
	char response[BUFSIZE];
	return sendCmd2SMS(motorID, GO_END_R, response, sizeof(response), s);
}

int go2negEC(Motorcodes motorID, SOCKET s)
{
	char response[BUFSIZE];
	return sendCmd2SMS(motorID, GO_END_L, response, sizeof(response), s);
}

int stopMotor(Motorcodes motorID, SOCKET s)
{
	char response[BUFSIZE];
	return sendCmd2SMS(motorID, STOP, response, sizeof(response), s);
}

void disconnectFromSMS(SOCKET s)
{
	// Shut down the client
	closesocket(s);
	WSACleanup();
	printf("\n[+] Disconnected From Server.\t(Shutdown)\n");
}

int main(void)
{
	SOCKET s;
	char response[BUFSIZE];
	int steps = 1000;
	int error = connect2SMS(smsIP, port, &s);
	Motorcodes m = MOTOR5;
	
	if (error == SOCKET_ERROR)
	{
		printf("[+] Can not Connect To Server.\t(ERROR)\n");
		return -1;
	}
	else
		printf("[+] Connected To Server.\t(Success)\n");

	//error = writeParams2SMS(, steps, STEPTYPE_1_1, RAMPE2, F1000Hz, s);
	int counter;
	Status st;
	char exit = '0';
	while (exit != 'q')
	{
		getState(m, &st, s);
	
		go2posEC(m, s);
		//go2zero(m, s);
		// runMotor(m, response, sizeof(response), 1000, s);
	/*	Sleep(1000);
		stopMotor(m, s);
	*/
		scanf( "%c", &exit);
	}
	zeroCounter(m, s);
	disconnectFromSMS(s);
	//system("pause");
 	return 0;
}
