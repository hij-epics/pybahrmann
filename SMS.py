# -*- coding: utf-8 -*-
"""
little SMS-Demo:

make Connection over Network
sending motor-parameter & move-command
reads motor-status

Created on Wed Jul 14 14:34:16 2021
new Version 1.1 created on 17.04.2023

@authors: schulze & bahrmann
"""

import SMSlib
import time

host = "192.168.55.120"
port = 5000

# Status von Motor5
status_motor5 = SMSlib.motorStatus(SMSlib.MOTOR5)
status_motor6 = SMSlib.motorStatus(SMSlib.MOTOR6)
# hier noch anderen Status von Motoren hinzufügen

# Voreinstellungen Motoren -> wird durch Lesen(GET_MOVEMENT) überschrieben!

motor5 = SMSlib.motorSetup(SMSlib.MOTOR5, SMSlib.STEPTYPE_1_1, SMSlib.RAMPE1, SMSlib.F1000Hz)
motor6 = SMSlib.motorSetup(SMSlib.MOTOR6, SMSlib.STEPTYPE_1_2, SMSlib.RAMPE1, SMSlib.F500Hz)


def status_position(status):
    return SMSlib.getState(status)


def status_short(status):
    return SMSlib.getState_short(status)


def run(motor, step):
    return SMSlib.runMotor(motor, step)


def init_motor(status, motor, select):
    error1 = SMSlib.getState(status)
    if error1 == 0:
        error1 = SMSlib.getName(motor)
    if error1 == 0 & select:                    # select = true -> read parameter from device
        error1 = SMSlib.getMovement(motor)
    if error1 == 0:
        error1 = SMSlib.getCurrent(motor)
    return error1

def print_motorparams(motor : SMSlib.motorSetup , status_motor : SMSlib.motorStatus):
    print('motor-parameter:')
    print('name->', motor.name)
    print('current->', motor.current, 'mA')
    print('frequency->', ((motor.frequency + 1) * 100), 'Hz')
    print('ramp->', motor.ramp)
    print('steptype->', SMSlib.steptype[motor.steptype >> 1])
    print('actual position:', status_motor.counter)

def move_motor_and_wait(motor : SMSlib.motorSetup , status_motor : SMSlib.motorStatus, steps: int):
        # Status prüfen
    if status_motor.motor_on == 0 & status_motor.endleft == 0 & status_motor.endright == 0:
        # definierte Anzahl Schritte mit eingestellten Parametern fahren
        error = run(motor, steps)
        if error == 0:
            print('Motor now drives', steps, 'steps!')
            state = 1
            # Zählerstand und Status abfragen bis Motor fertig
            while state == 1:
                error = status_position(status_motor)
                state = status_motor.motor_on
                # bei Fehler Abbruch
                if error == 1:
                    break
                else:
                    print('actual position:', status_motor.counter)
                    time.sleep(0.1)
            if error == 0:
                error = status_position(status_motor)
                print('end-position:', status_motor.counter)
                print('Motor has driven', steps, 'steps!')
    else:
        print('Motor is not ready or end contacts are triggered!')



# HAUPTPROGRAMM
#error = init()
error=SMSlib.connect(host, port)

if error == 0:
    # Motorstatus + Einstellungen + Parameter auslesen
    error = init_motor(status_motor5, motor5, True) & init_motor(status_motor6, motor6, True)


motor5 = SMSlib.motorSetup(SMSlib.MOTOR5, SMSlib.STEPTYPE_1_1, SMSlib.RAMPE1, SMSlib.F1000Hz)
motor6 = SMSlib.motorSetup(SMSlib.MOTOR6, SMSlib.STEPTYPE_1_1, SMSlib.RAMPE1, SMSlib.F200Hz)

if not error:

    # gelesene Werte anzeigen
    print_motorparams(motor5, status_motor5)
    print_motorparams(motor6, status_motor6)

    #run(motor5, 1000)
    #run(motor6, 1000)
    move_motor_and_wait(motor5, status_motor6, 1000)
    # time.sleep(0.3)
    # SMSlib.stopMotor(motor5)
    # SMSlib.stopMotor(motor6)

    
else:
    print('An error occurred during initialization!')    

SMSlib.disconnect()
