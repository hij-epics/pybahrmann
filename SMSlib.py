# -*- coding: utf-8 -*-
"""
SMS-Library for using SMS-CONTROLLER(UNI-JENA) over Network

Included only VC-1 Commands and Parameters !!!!

Created on Wed Jul 14 11:34:19 2021

Fixed Errors on 20.08.2021 -> Version 1.0
created on 17.04.2023 -> Finally Version 1.1
new in this version:
use status_class ->counter and status_flags
added new command: GET_MOVEMENT, GET_CURRENT, GET_NAME, GO_ZERO, GET_STATUS_SHORT

@authors: schulze & bahrmann
"""
import socket


s = socket.socket()
timeout = 1.0
s.settimeout(timeout)

# Befehlscodes
RUN_STEPS = 0x02
STOP = 0x03
RESET = 0x04
GO_END_L = 0x05
GO_END_R = 0x06
GET_STATUS = 0x07
GO_ZERO = 0x08
GET_CURRENT = 0x09
GET_STATUS_SHORT = 0x0A
GET_NAME = 0x0B
GET_MOVEMENT = 0x12

# Motorcodes
MOTOR1 = 0x10
MOTOR2 = 0x11
MOTOR3 = 0x12
MOTOR4 = 0x13
MOTOR5 = 0x14
MOTOR6 = 0x15
MOTOR7 = 0x16
MOTOR8 = 0x17
MOTOR9 = 0x18
MOTOR10 = 0x19
MOTOR11 = 0x1A
MOTOR12 = 0x1B
MOTOR13 = 0x1C
MOTOR14 = 0x1D
MOTOR15 = 0x1E
MOTOR16 = 0x1F
# Rampencodes
RAMPE0 = 0x00
RAMPE1 = 0x01
RAMPE2 = 0x02
RAMPE3 = 0x03
RAMPE4 = 0x04
RAMPE5 = 0x05
RAMPE6 = 0x06
RAMPE7 = 0x07
RAMPE8 = 0x08
RAMPE9 = 0x09
# Frequenzcodes
F100Hz = 0x00
F200Hz = 0x01
F300Hz = 0x02
F400Hz = 0x03
F500Hz = 0x04
F600Hz = 0x05
F700Hz = 0x06
F800Hz = 0x07
F900Hz = 0x08
F1000Hz = 0x09
F1100Hz = 0x0A
F1200Hz = 0x0B
F1300Hz = 0x0C
F1400Hz = 0x0D
F1500Hz = 0x0E
F1600Hz = 0x0F
F1700Hz = 0x10
F1800Hz = 0x11
F1900Hz = 0x12
F2000Hz = 0x13
F2100Hz = 0x14
F2200Hz = 0x15
F2300Hz = 0x16
F2400Hz = 0x17
F2500Hz = 0x18
F2600Hz = 0x19
F2700Hz = 0x1A
F2800Hz = 0x1B
F2900Hz = 0x1C
F3000Hz = 0x1D
F3100Hz = 0x1E
F3200Hz = 0x1F
F3300Hz = 0x20
F3400Hz = 0x21
F3500Hz = 0x22
F3600Hz = 0x23
F3700Hz = 0x24
F3800Hz = 0x25
F3900Hz = 0x26
F4000Hz = 0x27
# F4100 = 0x28
#  ...
# F25000Hz = 0xF9                       # -> erst ab VC-2 und SMC-2 Karten!
# Schrittartcodes
STEPTYPE_1_1 = 0x00
STEPTYPE_1_2 = 0x02
STEPTYPE_1_4 = 0x04
STEPTYPE_1_8 = 0x06
# STEPTYPE_1_16 = 0x40                   # -> erst ab VC-2 und SMC-2 Karten!
# STEPTYPE_1_32 = 0x41

# Schrittartzuordnung
steptype = ["1/1", "1/2", "1/4", "1/8"]
# Fehlerzuordnungstabelle
errors = [[0xE1, 0xE2, 0xE3, 0xE4, 0xE5, 0xE6, 0xE7, 0xE8, 0xE9, 0xEA, 0xEF, 0xFF, 0xF1, 0xF2, 0xF3, 0xF4],
          ["Steuerung nicht im PC-Modus",
           "Befehlsspeicher voll",
           "keine Daten vorhanden",
           "Motor noch aktiv",
           "falsches Datenformat",
           "Enkontakte ausgeloest",
           "falscher Befehl",
           "Modul/Motor nicht vorhanden",
           "Fernbedienung defekt",
           "Fehler bei der Datenuebertragung",
           "Kurzschluss Motor",
           "kein Fehler",
           "Verbindung zur Steuerung unterbrochen",
           "Client konnte nicht gefunden werden",
           "keine Steuerung gefunden",
           "Anzahl Schritte falsch"
           ]
          ]
# Hardware-Rückmeldungen(Software-Handshake)
handshake_list = [b'\xff', b'\xe1', b'\xe2', b'\xe3', b'\xe4', b'\xe5', b'\xe6', b'\xe7',
                  b'\xe8', b'\xe9', b'\xea', b'\xeb', b'\xec', b'\xed', b'\xee', b'\xef']
BUFSIZE = 1024


class motorStatus:
    def __init__(self, motor):
        self.motor = motor
        self.counter = "0"
        self.endleft = 0
        self.endright = 0
        self.motor_on = 0
        self.microstep_on = 0
        self.softend_on = 0


class motorSetup:
    def __init__(self, motor, steptype, ramp, frequency):
        self.motor = motor
        self.steptype = steptype
        self.ramp = ramp
        self.frequency = frequency
        self.current = 180                          # Strom in Steuerung nicht einstellbar(VC1), nur zum Lesen verwenden!
        self.name = "Username"

    def set_parameter(self, steptype, ramp, frequency):
        self.steptype = steptype
        self.ramp = ramp
        self.frequency = frequency


"""Daten zur Steuerung senden"""


def send_data(msg):
    fehler = 0
    # Nachricht senden
    s.send(msg)
    try:
        data = s.recv(BUFSIZE)
    # Fehlerauswertung falls Verbindung unterbrochen wird oder Client nicht antwortet
    except socket.timeout:
        s.close()
        return b'\xf1'
    

    found = -1
    while found == -1:
        # Softhandshake auswerten
        for i in handshake_list:
            found = data.find(i)
            if found != -1:
                if i != b'\xff':
                    fehler = i
                break
        if found == -1:
            # weiter auf Daten vom Server warten, wenn kein Handshake gefunden
            try:
                data += s.recv(BUFSIZE)
            # Fehlerauswertung falls Verbindung unterbrochen wird \ kein Handshake gefunden wird
            except socket.timeout:
                fehler = b'\xf1'
                s.close()
                return fehler
    # Daten anzeigen
    if fehler == 0:
        return data
    # Fehler anzeigen
    else:
        return fehler


"""Fehlermeldung anzeigen ->Codes auswerten"""


def errorMessage(errorcode):
    idx = errors[0].index(errorcode)
    message = errors[1][idx]
    return message


"""Parametersting erstellen und senden"""


def sendParameters(motor, steps):
    strHex = "%0.5X" % abs(steps)
    stepsHex = "0" + strHex[4] + "0" + strHex[3] + "0" + strHex[2] + "0" + strHex[1] + "0" + strHex[0]
    stepsBytes = bytes.fromhex(stepsHex)
    sendBytes = [motor.motor + 0x10]  # Motordatensteuerwort
    sendBytes.extend(list(stepsBytes))  # Anzahl Schritte
    if steps < 0:
        sendBytes.append(motor.steptype)  # negativer Wert + Schrittart
    else:
        sendBytes.append(motor.steptype + 0x01)  # positiver Wert + Schrittart
    sendBytes.append(motor.ramp)  # Rampe
    sendBytes.append(motor.frequency)  # Frequenz
    return send_data(bytes(sendBytes))


"""Motornamen abfragen"""


def getName(motor):
    error = 0  # Standardwerte einstellen
    mot = motor.motor
    answer = send_data(bytes([mot, GET_NAME]))  # Motorkommandosteuerwort + Kommando
    if len(answer) == 17:
        if answer[16] != 0xff:
            print("Motor error: " + errorMessage(0xE5))  # falsches Datenformat
            error = 1
        if error == 0:
            motor.name = answer[0:16].decode()
    else:
        print("Motor error: " + errorMessage(answer[0]))
        error = 1
    return error


"""Motorparameter(RFS) abfragen"""


def getMovement(motor):
    error = 0  # Standardwerte einstellen
    value = 0
    mot = motor.motor
    answer = send_data(bytes([mot, GET_MOVEMENT]))  # Motorkommandosteuerwort + Kommando
    if len(answer) == 4:
        if answer[3] != 0xff:
            print("Motor error: " + errorMessage(0xE5))  # falsches Datenformat
            error = 1
        if error == 0:
            if answer[0] & 0x80 & answer[1] & 0x80:  # VC-2 Format ->Transportbit  bei Frequenz löschen
                motor.frequency = answer[2] & 0x7F
            else:  # all others VC
                motor.frequency = answer[2]
                motor.ramp = answer[1]
                motor.steptype = (answer[0] & 0x06)     # nur bis 1/8 Schritt!!! ->1/16, 1/32 erst ab VC-2
                #                   VC-0/VC-1
    else:
        print("Motor error: " + errorMessage(answer[0]))
        error = 1
    return error


"""Motorstrom abfragen"""


def getCurrent(motor):
    error = 0  # Standardwerte einstellen
    value = 0
    mot = motor.motor
    answer = send_data(bytes([mot, GET_CURRENT]))  # Motorkommandosteuerwort + Kommando
    if len(answer) == 4:
        if answer[3] != 0xff:
            print("Motor error: " + errorMessage(0xE5))  # falsches Datenformat
            error = 1
        if error == 0:
            value = answer[0:2].decode()
            motor.current = int(value) * 25
    else:
        print("Motor error: " + errorMessage(answer[0]))
        error = 1
    return error


"""nur Status-Bytes abfragen"""


def getState_short(status):
    error = 0  # Standardwerte einstellen
    mot = status.motor
    answer = send_data(bytes([mot, GET_STATUS_SHORT]))  # Motorkommandosteuerwort + Kommando
    if len(answer) == 2:
        if answer[1] != 0xff:
            print("Motor error: " + errorMessage(0xE5))  # falsches Datenformat
            error = 1
        if error == 0:
            if answer[0] & 0x01:  # Endkontakt rechts
                status.endright = 1
            if answer[0] & 0x02:  # Endkontakt links
                status.endleft = 1
            if answer[0] & 0x08:  # Motorstatus -> 1= nicht bereit
                status.motor_on = 1
            else:
                status.motor_on = 0
            if answer[0] & 0x10:  # Microschitte aktiviert
                status.microstep_on = 1
            else:
                status.microstep_on = 0
            if answer[0] & 0x20:  # Softendkontakte aktiviert
                status.softend_on = 1
            else:
                status.softend_on = 0
    else:
        print("Motor error: " + errorMessage(answer[0]))
        error = 1
    return error


"""Zählerstand und Status-Bytes abfragen"""


def getState(status):
    error = 0  # Standardwerte einstellen
    pos = 0
    mot = status.motor
    answer = send_data(bytes([mot, GET_STATUS]))  # Motorkommandosteuerwort + Kommando
    if len(answer) == 10:
        if answer[9] != 0xff:
            print("Motor error: " + errorMessage(0xE5))  # falsches Datenformat
            error = 1
        if error == 0:
            pos = answer[0:6].decode()[::-1]    #Zahl von 0-6 + drehen
            if answer[7] & 0x01:  # Endkontakt rechts
                status.endright = 1
            if answer[7] & 0x02:  # Endkontakt links
                status.endleft = 1
            if answer[7] & 0x04:  # Vorzeichen
                pos = '-' + pos
            else:
                pos = '+' + pos
            if answer[7] & 0x08:  # Motorstatus -> 1= nicht bereit
                status.motor_on = 1
            else:
                status.motor_on = 0
            if answer[7] & 0x10:  # Microschitte aktiviert
                status.microstep_on = 1
            else:
                status.microstep_on = 0
            if answer[7] & 0x20:  # Softendkontakte aktiviert
                status.softend_on = 1
            else:
                status.softend_on = 0
            micro = answer[8]
            if micro >= 128:  # neue Variante Microschritte(32)
                micro = (micro - 128) / 32
            else:  # alte Variante Microschritte(8)
                micro = (micro - 48) / 4
            status.counter = pos + str(micro)[1:]  # Vorkommastelle löschen
    else:
        print("Motor error: " + errorMessage(answer[0]))
        error = 1
    return error


"""Motor anhalten"""


def stopMotor(motor):
    error = 0
    mot = motor.motor
    answer = send_data(bytes([mot, STOP]))
    if answer != b'\xff':
        print("Motor error: " + errorMessage(answer[0]))
        error = 1
    return error


"""Anzahl Schritte fahren"""


def runMotor(motor, steps):
    error = 0
    if steps > 1048575 or steps < -1048575 or steps == 0:
        print("Motor error: " + errorMessage(0xF4))
        error = 1
        return error
    answer = sendParameters(motor, steps)
    if answer != b'\xff':
        print("Motor error: " + errorMessage(answer[0]))
        error = 1
        return error
    else:
        answer = send_data(bytes([motor.motor, RUN_STEPS]))
        if answer != b'\xff':
            print("Motor error: " + errorMessage(answer[0]))
            error = 1
    return error


"""zum linken Endkontakt(-) fahren"""


def gotoEndL(motor):
    error = 0
    steps = -1048575  # maximale Anzahl(0xFFFFF) Schritte in negative Richtung
    answer = sendParameters(motor, steps)
    if answer != b'\xff':
        print("Motor error: " + errorMessage(answer[0]))
        error = 1
        return error
    else:
        answer = send_data(bytes([motor.motor, GO_END_L]))
        if answer != b'\xff':
            print("Motor error: " + errorMessage(answer[0]))
            error = 1
    return error


"""zur Nullposition(Zähler) fahren"""


def gotoZero(motor):
    error = 0
    steps = -1048575  # maximale Anzahl(0xFFFFF) Schritte in negative Richtung
    answer = sendParameters(motor, steps)
    if answer != b'\xff':
        print("Motor error: " + errorMessage(answer[0]))
        error = 1
        return error
    else:
        answer = send_data(bytes([motor.motor, GO_ZERO]))
        if answer != b'\xff':
            print("Motor error: " + errorMessage(answer[0]))
            error = 1
    return error

"""zum rechten Endkontakt(+) fahren"""


def gotoEndR(motor):
    error = 0
    steps = 1048575  # maximale Anzahl(0xFFFFF) Schritte in positve Richtung
    answer = sendParameters(motor, steps)
    if answer != b'\xff':
        print("Motor error: " + errorMessage(answer[0]))
        error = 1
        return error
    else:
        answer = send_data(bytes([motor.motor, GO_END_R]))
        if answer != b'\xff':
            print("Motor error: " + errorMessage(answer[0]))
            error = 1
    return error


"""Zählerstand auf Null setzen"""


def resetPosition(motor):
    error = 0
    mot = motor.motor
    answer = send_data(bytes([mot, RESET]))
    if answer != b'\xff':
        print("Motor error: " + errorMessage(answer[0]))
        error = 1
    return error


"""Verbindung zur Steuerung über Netzwerk herstellen"""


def connect(host, port):
    error = 0
    # Ein INet Streaming (TCP/IP) Socket erzeugen
    # s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    # Wartezeit für Verbindungsfehler
    s.settimeout(1)
    try:
        # Zum Server verbinden
        s.connect((host, port))
    except socket.timeout:
        print("Motor error: " + errorMessage(0xf2))
        error = 1
    # Seriennummer abfragen -> Test ob Verbindung ok
    answer = send_data(bytes('N', 'utf8'))

    if (len(answer) != 9) or (answer[8] != 0xff):
        print("Motor error: " + errorMessage(0xf3))
        error = 1
        # print(pos)
    else:
        sn = answer.strip(b'\xff').decode()
        print('Connected to Stepper Motor Controller at ' + str(host))
        print('With serial number ' + str(sn))
    return error


"""Netzwerk-Verbindung trennen"""


def disconnect():
    s.close()
