# -*- coding: utf-8 -*-
"""
little SMS-Demo:

make Connection over Network
sending motor-parameter & move-command
reads motor-status

Created on Wed Jul 14 14:34:16 2021
new Version 1.1 created on 17.04.2023

@authors: schulze & bahrmann
"""

import SMSlib
import time

host = "192.168.5.2"
port = 5000

# Status von Motor1
status_motor1 = SMSlib.motorStatus(SMSlib.MOTOR1)
status_motor2 = SMSlib.motorStatus(SMSlib.MOTOR2)
status_motor3 = SMSlib.motorStatus(SMSlib.MOTOR3)
status_motor4 = SMSlib.motorStatus(SMSlib.MOTOR4)
status_motor5 = SMSlib.motorStatus(SMSlib.MOTOR5)
status_motor6 = SMSlib.motorStatus(SMSlib.MOTOR6)
status_motor7 = SMSlib.motorStatus(SMSlib.MOTOR7)
status_motor8 = SMSlib.motorStatus(SMSlib.MOTOR8)
status_motor9 = SMSlib.motorStatus(SMSlib.MOTOR9)
status_motor10 = SMSlib.motorStatus(SMSlib.MOTOR10)
status_motor11 = SMSlib.motorStatus(SMSlib.MOTOR11)
status_motor12 = SMSlib.motorStatus(SMSlib.MOTOR12)
status_motor13 = SMSlib.motorStatus(SMSlib.MOTOR13)
status_motor14 = SMSlib.motorStatus(SMSlib.MOTOR14)
status_motor15 = SMSlib.motorStatus(SMSlib.MOTOR15)
status_motor16 = SMSlib.motorStatus(SMSlib.MOTOR16)

# Voreinstellungen Motoren -> wird durch Lesen(GET_MOVEMENT) überschrieben!
motor1 = SMSlib.motorSetup(SMSlib.MOTOR1, SMSlib.STEPTYPE_1_2, SMSlib.RAMPE1, SMSlib.F500Hz)
motor2 = SMSlib.motorSetup(SMSlib.MOTOR2, SMSlib.STEPTYPE_1_2, SMSlib.RAMPE1, SMSlib.F500Hz)
motor3 = SMSlib.motorSetup(SMSlib.MOTOR3, SMSlib.STEPTYPE_1_2, SMSlib.RAMPE1, SMSlib.F500Hz)
motor4 = SMSlib.motorSetup(SMSlib.MOTOR4, SMSlib.STEPTYPE_1_2, SMSlib.RAMPE1, SMSlib.F500Hz)
motor5 = SMSlib.motorSetup(SMSlib.MOTOR5, SMSlib.STEPTYPE_1_2, SMSlib.RAMPE1, SMSlib.F500Hz)
motor6 = SMSlib.motorSetup(SMSlib.MOTOR6, SMSlib.STEPTYPE_1_2, SMSlib.RAMPE1, SMSlib.F500Hz)
# bis hier für UNI-SMS
motor7 = SMSlib.motorSetup(SMSlib.MOTOR7, SMSlib.STEPTYPE_1_2, SMSlib.RAMPE1, SMSlib.F500Hz)
motor8 = SMSlib.motorSetup(SMSlib.MOTOR8, SMSlib.STEPTYPE_1_2, SMSlib.RAMPE1, SMSlib.F500Hz)
motor9 = SMSlib.motorSetup(SMSlib.MOTOR9, SMSlib.STEPTYPE_1_2, SMSlib.RAMPE1, SMSlib.F500Hz)
motor10 = SMSlib.motorSetup(SMSlib.MOTOR10, SMSlib.STEPTYPE_1_2, SMSlib.RAMPE1, SMSlib.F500Hz)
# bis hier für OPTO-1
motor11 = SMSlib.motorSetup(SMSlib.MOTOR11, SMSlib.STEPTYPE_1_2, SMSlib.RAMPE1, SMSlib.F500Hz)
motor12 = SMSlib.motorSetup(SMSlib.MOTOR12, SMSlib.STEPTYPE_1_2, SMSlib.RAMPE1, SMSlib.F500Hz)
motor13 = SMSlib.motorSetup(SMSlib.MOTOR13, SMSlib.STEPTYPE_1_2, SMSlib.RAMPE1, SMSlib.F500Hz)
motor14 = SMSlib.motorSetup(SMSlib.MOTOR14, SMSlib.STEPTYPE_1_2, SMSlib.RAMPE1, SMSlib.F500Hz)
motor15 = SMSlib.motorSetup(SMSlib.MOTOR15, SMSlib.STEPTYPE_1_2, SMSlib.RAMPE1, SMSlib.F500Hz)
motor16 = SMSlib.motorSetup(SMSlib.MOTOR16, SMSlib.STEPTYPE_1_2, SMSlib.RAMPE1, SMSlib.F500Hz)


# bis hier für UMC



def init():
    return SMSlib.connect(host, port)


def close():
    SMSlib.disconnect()


def status_position(status):
    return SMSlib.getState(status)


def status_short(status):
    return SMSlib.getState_short(status)


def run(motor, step):
    return SMSlib.runMotor(motor, step)


def init_motor(status, motor, select):
    error1 = SMSlib.getState(status)
    if error1 == 0:
        error1 = SMSlib.getName(motor)
    if error1 == 0 & select:                    # select = true -> read parameter from device
        error1 = SMSlib.getMovement(motor)
    if error1 == 0:
        error1 = SMSlib.getCurrent(motor)
    return error1


error = init()
if error == 0:
    # Motorstatus + Einstellungen + Parameter auslesen
    error = init_motor(status_motor4, motor4, True)
if not error:

    # gelesene Werte anzeigen

    print('motor-parameter:')
    print('name->', motor4.name)
    print('current->', motor4.current, 'mA')
    print('frequency->', ((motor4.frequency + 1) * 100), 'Hz')
    print('ramp->', motor4.ramp)
    print('steptype->', SMSlib.steptype[motor4.steptype >> 1])
    print('actual position:', status_motor4.counter)

    # Status prüfen
    if status_motor4.motor_on == 0 & status_motor4.endleft == 0 & status_motor4.endright == 0:
        # definierte Anzahl Schritte mit eingestellten Parametern fahren
        steps = -1000
        error = run(motor4, steps)
        if error == 0:
            print('Motor now drives', steps, 'steps!')
            state = 1
            # Zählerstand und Status abfragen bis Motor fertig
            while state == 1:
                error = status_position(status_motor4)
                state = status_motor4.motor_on
                # bei Fehler Abbruch
                if error == 1:
                    break
                else:
                    print('actual position:', status_motor4.counter)
                    time.sleep(0.1)
            if error == 0:
                error = status_position(status_motor4)
                print('end-position:', status_motor4.counter)
                print('Motor has driven', steps, 'steps!')
    else:
        print('Motor is not ready or end contacts are triggered!')

else:
    print('An error occurred during initialization!')
close()
