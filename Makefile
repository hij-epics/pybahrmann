CC = gcc
CFLAGS = -Wall -Wextra
LDFLAGS = -L"C:/msys64/ucrt64/lib/" -lws2_32


SRC_DIR = $(CURDIR)
OBJ_DIR = $(CURDIR)/obj
BIN_DIR = $(SRC_DIR)

SRCS = $(wildcard $(SRC_DIR)/*.cpp)
OBJS = $(patsubst $(SRC_DIR)/%.cpp,$(OBJ_DIR)/%.o,$(SRCS))
DEPS = $(OBJS:.o=.d)
TARGET = $(BIN_DIR)/sms.exe

all: $(TARGET)

$(TARGET): $(OBJS)
	$(CC) $(CFLAGS) $(LDFLAGS) $^ -o $@

$(OBJ_DIR)/%.o: $(SRC_DIR)/%.cpp
	$(CC) $(CFLAGS) -MMD -c $< -o $@

-include $(DEPS)

clean:
	rm -f $(OBJS) $(DEPS) $(TARGET)

.PHONY: all clean
