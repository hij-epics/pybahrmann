#ifndef _SMSLIB_H_
#define _SMSLIB_H_

#define end -1
#define FLAGS 0


typedef struct {
	int motor;
	bool endright;
	bool endleft;
	bool motor_on;
	bool microstep_on;
	bool softend_on;
	int position;
} Status;

enum Commandcodes {
	GET_COUNTER = 0x01, // Daf�r wird getStatus genutzt
	RUN_STEPS = 0x02,	// OK
	STOP = 0x03,		// OK	
	RESET = 0x04,		// zeroCounter, OK
	GO_END_L = 0x05,	// OK
	GO_END_R = 0x06,	// OK
	GET_STATUS = 0x07,  // OK
	GO_ZERO = 0x08,     // OK
	GET_CURRENT = 0x09, //Nicht impelemtiert
	GET_STATUS_SHORT = 0x0A, //Nicht impelemtiert
	GET_NAME = 0x0B,	//Nicht impelemtiert
	GET_MOVEMENT = 0x12 //Nicht impelemtiert
};

enum Stepcodes {
	STEPTYPE_1_1 = 0x00,
	STEPTYPE_1_2 = 0x02,
	STEPTYPE_1_4 = 0x04,
	STEPTYPE_1_8 = 0x06
};

enum Motorcodes {
	MOTOR1 = 0x10,
	MOTOR2 = 0x11,
	MOTOR3 = 0x12,
	MOTOR4 = 0x13,
	MOTOR5 = 0x14,
	MOTOR6 = 0x15,
	MOTOR7 = 0x16,
	MOTOR8 = 0x17,
	MOTOR9 = 0x18,
	MOTOR10 = 0x19,
	MOTOR11 = 0x1A,
	MOTOR12 = 0x1B,
	MOTOR13 = 0x1C,
	MOTOR14 = 0x1D,
	MOTOR15 = 0x1E,
	MOTOR16 = 0x1F
};


enum Frequenzcodes {
	F100Hz = 0x00,
	F200Hz = 0x01,
	F300Hz = 0x02,
	F400Hz = 0x03,
	F500Hz = 0x04,
	F600Hz = 0x05,
	F700Hz = 0x06,
	F800Hz = 0x07,
	F900Hz = 0x08,
	F1000Hz = 0x09,
	F1100Hz = 0x0A,
	F1200Hz = 0x0B,
	F1300Hz = 0x0C,
	F1400Hz = 0x0D,
	F1500Hz = 0x0E,
	F1600Hz = 0x0F,
	F1700Hz = 0x10,
	F1800Hz = 0x11,
	F1900Hz = 0x12,
	F2000Hz = 0x13,
	F2100Hz = 0x14,
	F2200Hz = 0x15,
	F2300Hz = 0x16,
	F2400Hz = 0x17,
	F2500Hz = 0x18,
	F2600Hz = 0x19,
	F2700Hz = 0x1A,
	F2800Hz = 0x1B,
	F2900Hz = 0x1C,
	F3000Hz = 0x1D,
	F3100Hz = 0x1E,
	F3200Hz = 0x1F,
	F3300Hz = 0x20,
	F3400Hz = 0x21,
	F3500Hz = 0x22,
	F3600Hz = 0x23,
	F3700Hz = 0x24,
	F3800Hz = 0x25,
	F3900Hz = 0x26,
	F4000Hz = 0x27
};

enum Rampencodes {
	RAMPE0 = 0x00,
	RAMPE1 = 0x01,
	RAMPE2 = 0x02,
	RAMPE3 = 0x03,
	RAMPE4 = 0x04,
	RAMPE5 = 0x05,
	RAMPE6 = 0x06,
	RAMPE7 = 0x07,
	RAMPE8 = 0x08,
	RAMPE9 = 0x09
};

#endif